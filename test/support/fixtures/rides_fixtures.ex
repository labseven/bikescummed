defmodule BikeWhere.RidesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `BikeWhere.Rides` context.
  """

  @doc """
  Generate a ride.
  """
  def ride_fixture(attrs \\ %{}) do
    {:ok, ride} =
      attrs
      |> Enum.into(%{
        description: "some description",
        end_at: ~N[2022-08-07 02:25:00],
        shift_id: "some shift_id",
        start_at: ~N[2022-08-07 02:25:00],
        title: "some title"
      })
      |> BikeWhere.Rides.create_ride()

    ride
  end
end
