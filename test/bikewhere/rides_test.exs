defmodule BikeWhere.RidesTest do
  use BikeWhere.DataCase

  alias BikeWhere.Rides

  describe "rides" do
    alias BikeWhere.Rides.Ride

    import BikeWhere.RidesFixtures

    @invalid_attrs %{description: nil, end_at: nil, shift_id: nil, start_at: nil, title: nil}

    test "list_rides/0 returns all rides" do
      ride = ride_fixture()
      assert Rides.list_rides() == [ride]
    end

    test "get_ride!/1 returns the ride with given id" do
      ride = ride_fixture()
      assert Rides.get_ride!(ride.id) == ride
    end

    test "create_ride/1 with valid data creates a ride" do
      valid_attrs = %{
        description: "some description",
        end_at: ~N[2022-08-07 02:25:00],
        shift_id: "some shift_id",
        start_at: ~N[2022-08-07 02:25:00],
        title: "some title"
      }

      assert {:ok, %Ride{} = ride} = Rides.create_ride(valid_attrs)
      assert ride.description == "some description"
      assert ride.end_at == ~N[2022-08-07 02:25:00]
      assert ride.shift_id == "some shift_id"
      assert ride.start_at == ~N[2022-08-07 02:25:00]
      assert ride.title == "some title"
    end

    test "create_ride/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Rides.create_ride(@invalid_attrs)
    end

    test "update_ride/2 with valid data updates the ride" do
      ride = ride_fixture()

      update_attrs = %{
        description: "some updated description",
        end_at: ~N[2022-08-08 02:25:00],
        shift_id: "some updated shift_id",
        start_at: ~N[2022-08-08 02:25:00],
        title: "some updated title"
      }

      assert {:ok, %Ride{} = ride} = Rides.update_ride(ride, update_attrs)
      assert ride.description == "some updated description"
      assert ride.end_at == ~N[2022-08-08 02:25:00]
      assert ride.shift_id == "some updated shift_id"
      assert ride.start_at == ~N[2022-08-08 02:25:00]
      assert ride.title == "some updated title"
    end

    test "update_ride/2 with invalid data returns error changeset" do
      ride = ride_fixture()
      assert {:error, %Ecto.Changeset{}} = Rides.update_ride(ride, @invalid_attrs)
      assert ride == Rides.get_ride!(ride.id)
    end

    test "delete_ride/1 deletes the ride" do
      ride = ride_fixture()
      assert {:ok, %Ride{}} = Rides.delete_ride(ride)
      assert_raise Ecto.NoResultsError, fn -> Rides.get_ride!(ride.id) end
    end

    test "change_ride/1 returns a ride changeset" do
      ride = ride_fixture()
      assert %Ecto.Changeset{} = Rides.change_ride(ride)
    end
  end
end
