defmodule BikeWhere.Rides.Ride do
  use Ecto.Schema
  import Ecto.Changeset

  schema "rides" do
    field :description, :string
    field :end_at, :naive_datetime
    field :shift_id, :string
    field :start_at, :naive_datetime
    field :title, :string
    field :owner_id, :id

    timestamps()
  end

  @doc false
  def changeset(ride, attrs) do
    ride
    |> cast(attrs, [:title, :start_at, :shift_id, :description])
    |> validate_required([:title, :start_at])
  end
end
