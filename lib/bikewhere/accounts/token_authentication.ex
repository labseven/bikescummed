defmodule BikeWhere.TokenAuthentication do
  @moduledoc """
      Service with functions for creating and signing in with magic link tokens.
  """

  import Ecto.Query, only: [where: 3]

  alias Phoenix.Token

  alias BikeWhere.Accounts.{User, AuthToken}
  alias BikeWhere.{AuthenticationEmail, Repo, Mailer}
  alias BikeWhereWeb.Endpoint

  @token_max_age 3_600

  @doc """
      Create and send a new magic link token to the user or email.
  """
  def provide_token(nil), do: {:error, :not_found}

  def provide_token(email) when is_binary(email) do
    User
    |> Repo.get_by(email: email)
    |> send_token()
  end

  def provide_token(user = %User{}) do
    send_token(user)
  end

  @doc """
      Checks the token validity.
  """
  def verify_token_value(value) do
    AuthToken
    |> where([t], t.value == ^value)
    |> Repo.one()
    |> verify_token()
  end

  # Token could not be found
  defp verify_token(nil), do: {:error, :invalid}

  defp verify_token(token) do
    token =
      token
      |> Repo.preload(:user)
      |> Repo.delete!()

    user_id = token.user.id

    # Verify token matching user id
    case Token.verify(Endpoint, "user", token.value, max_age: @token_max_age) do
      {:ok, ^user_id} ->
        {:ok, token.user}

      {:error, reason} ->
        {:error, reason}
    end
  end

  # User could not be found by email.
  defp send_token(nil), do: {:error, :not_found}

  # Create a token and send it to the user
  defp send_token(user) do
    user
    |> create_token()
    |> AuthenticationEmail.login_link(user)
    |> Mailer.deliver_now()

    {:ok, user}
  end

  # Create a new token for the given user and return the token value
  defp create_token(user) do
    changeset = AuthToken.changeset(%AuthToken{}, user)
    auth_token = Repo.insert!(changeset)
    auth_token.value
  end
end
