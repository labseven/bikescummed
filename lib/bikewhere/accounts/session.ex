defmodule BikeWhere.Accounts.Session do
  import Plug.Conn
  alias BikeWhere.Repo
  alias BikeWhere.Accounts.User

  def init(opts), do: opts

  def call(conn, _opts) do
    user_id = get_session(conn, :user_id)

    if user_id do
      current_user = Repo.get!(User, user_id)
      assign(conn, :current_user, current_user)
    else
      assign(conn, :current_user, nil)
    end
  end
end
