defmodule BikeWhere.Repo do
  use Ecto.Repo,
    otp_app: :bikewhere,
    adapter: Ecto.Adapters.Postgres
end
