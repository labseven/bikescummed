defmodule BikeWhere.AuthenticationEmail do
  use Bamboo.Phoenix, view: BikeWhere.EmailView

  import Bamboo.Email

  @doc """
    The signin email.
  """
  def login_link(token_value, user) do
    new_email()
    |> to(user.email)
    |> from("notifications@mail.verynice.party")
    |> subject("BikeWhere: Login Link")
    |> assign(:token, token_value)
    |> assign(:name, user.name)
    |> render("login_link.text")
  end
end
