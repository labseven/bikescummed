defmodule BikeWhereWeb.Router do
  use BikeWhereWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {BikeWhereWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :with_session do
    plug BikeWhere.Accounts.Session
  end

  scope "/", BikeWhereWeb do
    pipe_through :browser
    pipe_through :with_session

    get "/", PageController, :index

    resources "/users", UserController, only: [:show, :edit, :new, :create, :update]

    get "/signin/:token", SessionController, :show, as: :signin
    get "/signin", SessionController, :new
    post "/signin", SessionController, :create
    delete "/signout", SessionController, :delete

    live "/rides", RideLive.Index, :index
    live "/rides/new", RideLive.Index, :new
    live "/rides/:id/edit", RideLive.Index, :edit

    live "/rides/:id", RideLive.Show, :show
    live "/rides/:id/show/edit", RideLive.Show, :edit
  end

  # Other scopes may use custom stacks.
  # scope "/api", BikeWhereWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: BikeWhereWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
