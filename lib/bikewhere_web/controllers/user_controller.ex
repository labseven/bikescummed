defmodule BikeWhereWeb.UserController do
  use BikeWhereWeb, :controller

  alias BikeWhere.{Accounts, TokenAuthentication}
  alias BikeWhere.Accounts.User

  plug :not_loggedin when action in [:new]
  plug :authenticate_user when action in [:edit, :update, :delete]
  plug :authorize_user when action in [:edit, :update, :delete]

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.create_user(user_params) do
      {:ok, user} ->
        TokenAuthentication.provide_token(user)

        conn
        |> put_flash(:info, "You are signed up! Check your email to login.")
        |> redirect(to: Routes.ride_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    render(conn, "show.html", user: user)
  end

  def edit(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    changeset = Accounts.change_user(user)
    render(conn, "edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Accounts.get_user!(id)

    case Accounts.update_user(user, user_params) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "User updated successfully.")
        |> redirect(to: Routes.user_path(conn, :show, user))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", user: user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    {:ok, _user} = Accounts.delete_user(user)

    conn
    |> put_flash(:info, "User deleted successfully.")
    |> redirect(to: Routes.user_path(conn, :index))
  end

  defp not_loggedin(conn, _params) do
    if conn.assigns.current_user != nil do
      conn
      |> put_flash(:error, "You are already signed in!")
      |> redirect(to: Routes.user_path(conn, :show, conn.assigns.current_user))
      |> halt()
    else
      conn
    end
  end

  defp authenticate_user(conn, _params) do
    if conn.assigns.current_user != nil do
      conn
    else
      conn
      |> put_flash(:error, "You need to be signed in to do that!")
      |> redirect(to: Routes.session_path(conn, :new))
      |> halt()
    end
  end

  defp authorize_user(conn, _params) do
    %{params: %{"id" => user_id}} = conn

    if conn.assigns.current_user.id == String.to_integer(user_id) do
      conn
    else
      conn
      |> put_flash(:error, "You cannot edit other users!")
      |> redirect(to: Routes.ride_path(conn, :index))
      |> halt()
    end
  end
end
