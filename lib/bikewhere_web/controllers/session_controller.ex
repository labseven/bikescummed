defmodule BikeWhereWeb.SessionController do
  use BikeWhereWeb, :controller

  alias BikeWhere.TokenAuthentication

  plug :not_loggedin when action in [:new, :show]

  def new(conn, _params) do
    render(conn, "new.html")
  end

  @doc """
    Generate and send magic link.
  """
  def create(conn, %{"session" => %{"email" => email}}) do
    TokenAuthentication.provide_token(email)

    conn
    |> put_flash(:info, "Login link sent to #{email}! Check your inbox to login.")
    |> redirect(to: Routes.ride_path(conn, :index))
  end

  @doc """
    Login user with magic link token.
    Sets the given user as 'current_user' and updates the session
  """
  def show(conn, %{"token" => token}) do
    case TokenAuthentication.verify_token_value(token) do
      {:ok, user} ->
        conn
        |> assign(:current_user, user)
        |> put_session(:user_id, user.id)
        |> configure_session(renew: true)
        |> put_flash(:info, "Welcome back #{user.name}")
        |> redirect(to: Routes.user_path(conn, :show, user))

      {:error, _reason} ->
        conn
        |> put_flash(:error, "Sorry, this login link is invalid. Send another?")
        |> redirect(to: Routes.session_path(conn, :new))
    end
  end

  def delete(conn, _params) do
    conn
    |> assign(:current_user, nil)
    |> configure_session(drop: true)
    |> delete_session(:user_id)
    |> put_flash(:info, "Logged out. Bye!")
    |> redirect(to: Routes.ride_path(conn, :index))
  end

  defp not_loggedin(conn, _params) do
    if conn.assigns.current_user != nil do
      conn
      |> put_flash(:error, "You are already signed in!")
      |> redirect(to: Routes.user_path(conn, :show, conn.assigns.current_user))
      |> halt()
    else
      conn
    end
  end
end
