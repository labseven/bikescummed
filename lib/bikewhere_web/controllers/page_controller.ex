defmodule BikeWhereWeb.PageController do
  use BikeWhereWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
