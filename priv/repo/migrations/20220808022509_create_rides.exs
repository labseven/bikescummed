defmodule BikeWhere.Repo.Migrations.CreateRides do
  use Ecto.Migration

  def change do
    create table(:rides) do
      add :title, :string
      add :start_at, :naive_datetime
      add :end_at, :naive_datetime
      add :shift_id, :string
      add :description, :text
      add :owner_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:rides, [:owner_id])
  end
end
